# rsa-algorithm

Este projeto refere-se a implementação do algoritmo de criptografia
assíncrona RSA.

Para gerar um `.jar` executável siga os seguintes passos:

- certifique-se de ter o maven e o git instalado em sua máquina;
- abra seu terminal na pasta onde desejas baixar o projeto e insira
o comando `git clone https://pedroviniv@bitbucket.org/pedroviniv/rsa-example.git`;
- entre navegue até a pasta `rsa-example`;
- ainda no seu terminal execute o comando `mvn install`;
- uma pasta `target` será gerada e lá estará o jar executável gerado;

Por fim, para executar o jar gerado:

- navegue até a pasta `target` e no terminal, execute o comando `java -jar rsa-example.jar`;

