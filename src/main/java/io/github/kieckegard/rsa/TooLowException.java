/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.rsa;

/**
 *
 * @author kieckegard
 */
public class TooLowException extends IllegalArgumentException {

    public TooLowException(String s) {
        super(s);
    }
}
