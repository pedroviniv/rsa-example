/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.rsa;

/**
 *
 * @author kieckegard
 */
public class NotAPrimeException extends IllegalArgumentException {

    public NotAPrimeException(String s) {
        super(s);
    }

    public NotAPrimeException(String message, Throwable cause) {
        super(message, cause);
    }

}
