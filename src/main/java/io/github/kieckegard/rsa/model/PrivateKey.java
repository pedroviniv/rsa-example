/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.rsa.model;

import java.math.BigInteger;

/**
 *
 * @author kieckegard
 */
public class PrivateKey {
    
    private BigInteger d;
    private BigInteger N;

    public PrivateKey(BigInteger d, BigInteger N) {
        this.d = d;
        this.N = N;
    }

    public PrivateKey() {
    }

    public BigInteger getD() {
        return d;
    }

    public void setD(BigInteger d) {
        this.d = d;
    }

    public BigInteger getN() {
        return N;
    }

    public void setN(BigInteger N) {
        this.N = N;
    }

    @Override
    public String toString() {
        return "PrivateKey{" + "d=" + d + ", N=" + N + '}';
    }
}
