/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package io.github.kieckegard.rsa;

import io.github.kieckegard.rsa.model.PublicKey;
import io.github.kieckegard.rsa.model.PrivateKey;
import io.github.kieckegard.rsa.model.Keys;
import java.math.BigInteger;
import java.util.Random;

/**
 *
 * @author kieckegard
 */
public class RSA {
    
    private BigInteger p;
    private BigInteger q;
    private BigInteger N;
    private BigInteger phi;
    private BigInteger e;
    private BigInteger d;
    private Random     r;
    
    private BigInteger greater(BigInteger d, BigInteger e) {
        return d.compareTo(e) > 0 ? d : e;
    }
    
    private void validate(BigInteger p, BigInteger q) {
        boolean valid = Primes.isPrime(p.intValue()) 
                && Primes.isPrime(q.intValue());
        if(!valid)
            throw new NotAPrimeException("p and q must be primes!");
    }
    
    private void validate(int bitLength) {
        if(bitLength < 8)
            throw new TooLowException("p and q are too low. Please choose"
                    + " a 8 bits number or greater.");
    }
 
    /**
     * 
     * @param p 
     * @param q 
     * @throws NotAPrimeException - when p or q is not prime
     */
    public RSA(BigInteger p, BigInteger q) {
        
        validate(p,q);
        //
        r = new Random();
        this.p = p;
        this.q = q;
        N = p.multiply(q);
        phi = p.subtract(BigInteger.ONE).multiply(q.subtract(BigInteger.ONE));
        int bitLenght = greater(p,q).bitLength();
        validate(bitLenght);
        e = BigInteger
                .probablePrime(bitLenght, r);
        while (phi.gcd(e).compareTo(BigInteger.ONE) > 0 && e.compareTo(phi) < 0) {
            e.add(BigInteger.ONE);
        }
        d = e.modInverse(phi);
    }
    
    /**
     * Wrapps the publickey and the privatekey in an object called Keys
     * @return instance of Keys obj
     */
    public Keys getKeys() {
        return new Keys(new PublicKey(e, N), new PrivateKey(d, N));
    }
    
    /**
     * encrypt de message accordingly to the public key passed by param 
     * @param message text to be encrypted
     * @param p public key
     * @return 
     */
    public long[] encrypt(String message, PublicKey p) {
        int length = message.length();
        long[] result = new long[length];
        for(int i = 0; i < length; i++) {
            result[i] = BigInteger
                    .valueOf((int) message.charAt(i))
                    .modPow(p.getE(), p.getN())
                    .longValue();
        }
        return result;
    }
 
    /**
     * Decrypt the message accordingly to the private key passed by param
     * @param message encrypted message to be decrypted
     * @param p private eky
     * @return 
     */
    public String decrypt(long[] message, PrivateKey p) {
        
        int length = message.length;
        StringBuilder strBuilder = new StringBuilder();
        
        for(int i = 0; i < length; i++) {
            char currentChar = (char) BigInteger
                    .valueOf(message[i])
                    .modPow(p.getD(), p.getN())
                    .intValue();
            strBuilder.append(currentChar);
        }
        
        return strBuilder.toString();
    }

    public BigInteger getP() {
        return p;
    }

    public BigInteger getQ() {
        return q;
    }

    public BigInteger getN() {
        return N;
    }

    public BigInteger getPhi() {
        return phi;
    }

    public BigInteger getE() {
        return e;
    }

    public BigInteger getD() {
        return d;
    }

    public Random getR() {
        return r;
    }
}
